\contentsline {chapter}{Abstract}{2}{Doc-Start}
\contentsline {chapter}{Acknowledgements}{3}{Doc-Start}
\contentsline {chapter}{\numberline {1}Gi\IeC {\'\ohorn }i thi\IeC {\d \ecircumflex }u \IeC {\dj }\IeC {\`\ecircumflex } t\IeC {\`a}i}{10}{chapter.1}
\contentsline {section}{\numberline {1.1}\IeC {\DJ }\IeC {\d \abreve }t v\IeC {\'\acircumflex }n \IeC {\dj }\IeC {\`\ecircumflex } b\IeC {\`a}i to\IeC {\'a}n}{10}{section.1.1}
\contentsline {section}{\numberline {1.2}B\IeC {\'\ocircumflex } c\IeC {\d u}c \IeC {\dj }\IeC {\`\ocircumflex } \IeC {\'a}n}{12}{section.1.2}
\contentsline {chapter}{\numberline {2}T\IeC {\h \ocircumflex }ng quan c\IeC {\'a}c nghi\IeC {\^e}n c\IeC {\'\uhorn }u v\IeC {\`a} c\IeC {\'a}c ki\IeC {\'\ecircumflex }n th\IeC {\'\uhorn }c c\IeC {\ohorn } s\IeC {\h \ohorn }}{13}{chapter.2}
\contentsline {section}{\numberline {2.1}\IeC {\DJ }i\IeC {\d \ecircumflex }n to\IeC {\'a}n \IeC {\dj }\IeC {\'a}m m\IeC {\^a}y}{13}{section.2.1}
\contentsline {section}{\numberline {2.2}B\IeC {\`a}i to\IeC {\'a}n t\IeC {\d \uhorn } \IeC {\dj }\IeC {\d \ocircumflex }ng m\IeC {\h \ohorn } r\IeC {\d \ocircumflex }ng}{17}{section.2.2}
\contentsline {section}{\numberline {2.3}C\IeC {\'a}c ph\IeC {\uhorn }\IeC {\ohorn }ng ph\IeC {\'a}p d\IeC {\d \uhorn } \IeC {\dj }o\IeC {\'a}n chu\IeC {\~\ocircumflex }i th\IeC {\`\ohorn }i gian}{21}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}T\IeC {\h \ocircumflex }ng quan}{21}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}C\IeC {\'a}c ph\IeC {\uhorn }\IeC {\ohorn }ng ph\IeC {\'a}p tuy\IeC {\'\ecircumflex }n t\IeC {\'\i }nh}{23}{subsection.2.3.2}
\contentsline {subsubsection}{\numberline {2.3.2.1}M\IeC {\^o} h\IeC {\`\i }nh t\IeC {\d \uhorn } h\IeC {\`\ocircumflex }i quy (Auto Regressive)}{23}{subsubsection.2.3.2.1}
\contentsline {subsubsection}{\numberline {2.3.2.2}M\IeC {\^o} h\IeC {\`\i }nh trung b\IeC {\`\i }nh tr\IeC {\uhorn }\IeC {\d \ohorn }t (Moving average)}{24}{subsubsection.2.3.2.2}
\contentsline {subsubsection}{\numberline {2.3.2.3}M\IeC {\^o} h\IeC {\`\i }nh l\IeC {\`a}m m\IeC {\d i}n theo h\IeC {\`a}m s\IeC {\'\ocircumflex } m\IeC {\~u}}{25}{subsubsection.2.3.2.3}
\contentsline {subsection}{\numberline {2.3.3}M\IeC {\^o} h\IeC {\`\i }nh m\IeC {\d a}ng n\IeC {\ohorn }-ron nh\IeC {\^a}n t\IeC {\d a}o}{26}{subsection.2.3.3}
\contentsline {subsection}{\numberline {2.3.4}Ph\IeC {\uhorn }\IeC {\ohorn }ng ph\IeC {\'a}p h\IeC {\d o}c s\IeC {\^a}u}{28}{subsection.2.3.4}
\contentsline {subsubsection}{\numberline {2.3.4.1}Ph\IeC {\uhorn }\IeC {\ohorn }ng ph\IeC {\'a}p m\IeC {\d a}ng n\IeC {\ohorn }-ron h\IeC {\`\ocircumflex }i quy}{28}{subsubsection.2.3.4.1}
\contentsline {subsubsection}{\numberline {2.3.4.2}M\IeC {\d a}ng Long Short Term Memory Neural Network}{30}{subsubsection.2.3.4.2}
\contentsline {subsection}{\numberline {2.3.5}Ph\IeC {\uhorn }\IeC {\ohorn }ng ph\IeC {\'a}p chu\IeC {\~\ocircumflex }i th\IeC {\`\ohorn }i gian m\IeC {\`\ohorn }}{31}{subsection.2.3.5}
\contentsline {section}{\numberline {2.4}Ph\IeC {\uhorn }\IeC {\ohorn }ng ph\IeC {\'a}p ph\IeC {\^a}n t\IeC {\'\i }ch m\IeC {\'\ocircumflex }i t\IeC {\uhorn }\IeC {\ohorn }ng quan nhi\IeC {\`\ecircumflex }u chi\IeC {\`\ecircumflex }u}{33}{section.2.4}
\contentsline {chapter}{\numberline {3}M\IeC {\^o} h\IeC {\`\i }nh \IeC {\dj }\IeC {\`\ecircumflex } xu\IeC {\'\acircumflex }t}{36}{chapter.3}
\contentsline {section}{\numberline {3.1}T\IeC {\h \ocircumflex }ng quan m\IeC {\^o} h\IeC {\`\i }nh}{36}{section.3.1}
\contentsline {section}{\numberline {3.2}Ti\IeC {\`\ecircumflex }n x\IeC {\h \uhorn } l\IeC {\'y} d\IeC {\~\uhorn } li\IeC {\d \ecircumflex }u}{37}{section.3.2}
\contentsline {section}{\numberline {3.3}L\IeC {\d \uhorn }a ch\IeC {\d o}n \IeC {\dj }\IeC {\d \abreve }c tr\IeC {\uhorn }ng}{38}{section.3.3}
\contentsline {section}{\numberline {3.4}Hu\IeC {\'\acircumflex }n luy\IeC {\d \ecircumflex }n m\IeC {\^o} h\IeC {\`\i }nh}{39}{section.3.4}
\contentsline {section}{\numberline {3.5}D\IeC {\d \uhorn } \IeC {\dj }o\IeC {\'a}n}{40}{section.3.5}
\contentsline {chapter}{\numberline {4}Th\IeC {\d \uhorn }c nghi\IeC {\d \ecircumflex }m v\IeC {\`a} \IeC {\dj }\IeC {\'a}nh gi\IeC {\'a}}{42}{chapter.4}
\contentsline {section}{\numberline {4.1}C\IeC {\`a}i \IeC {\dj }\IeC {\d \abreve }t th\IeC {\d \uhorn }c nghi\IeC {\d \ecircumflex }m}{42}{section.4.1}
\contentsline {section}{\numberline {4.2}M\IeC {\`\ohorn } h\IeC {\'o}a d\IeC {\~\uhorn } li\IeC {\d \ecircumflex }u}{43}{section.4.2}
\contentsline {section}{\numberline {4.3}L\IeC {\d \uhorn }a ch\IeC {\d o}n \IeC {\dj }\IeC {\d \abreve }c tr\IeC {\uhorn }ng}{44}{section.4.3}
\contentsline {section}{\numberline {4.4}D\IeC {\d \uhorn } \IeC {\dj }o\IeC {\'a}n}{46}{section.4.4}
\contentsline {chapter}{\numberline {5}K\IeC {\'\ecircumflex }t lu\IeC {\d \acircumflex }n v\IeC {\`a} h\IeC {\uhorn }\IeC {\'\ohorn }ng ph\IeC {\'a}t tri\IeC {\h \ecircumflex }n}{50}{chapter.5}
